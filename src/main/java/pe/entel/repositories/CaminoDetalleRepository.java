package pe.entel.repositories;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pe.entel.entities.CaminoDetalle;

@RepositoryRestResource(path = "caminodetalles", collectionResourceRel = "caminodetalles")
public interface CaminoDetalleRepository extends JpaRepository<CaminoDetalle, Long> {
    List<CaminoDetalle> findByCaminoIdIn(List<Long> ids);
}
