/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.repositories;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pe.entel.entities.Nodo;

/**
 *
 * @author dmontjoy
 */
@RepositoryRestResource(path = "nodos", collectionResourceRel = "nodos")
public interface NodoRepository extends JpaRepository<Nodo, Long>{
    @RestResource(path = "allByNodoNombre", rel="allByNodoNombre")
    Set<Nodo> findAllByNombreContaining(@Param("nodoNombre") String term);
    
    Set<Nodo> findByIdIn(List<Long> ids);
}
