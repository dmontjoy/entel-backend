/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.repositories.jdbc;

import java.util.List;
import pe.entel.entities.Camino;

/**
 *
 * @author dmontjoy
 */
public interface CaminoJdbcRepository {
    
    List<Camino> findAllByFilter(List<String> idNodos);
    
}
