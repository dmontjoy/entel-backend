/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.repositories.jdbc.impl;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.entel.entities.Camino;
import pe.entel.mapper.CaminoMapper;
import pe.entel.repositories.jdbc.CaminoJdbcRepository;
import pe.entel.repositories.jdbc.CustomDataSource;

/**
 *
 * @author dmontjoy
 */
@Repository(value = "caminoJdbcRepository")
public class CaminoJdbcRepositoryImpl extends CustomDataSource implements CaminoJdbcRepository{

    @Override
    public List<Camino> findAllByFilter(List<String> idNodos) {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(getJdbcTemplate())
                .withProcedureName("getCaminos")
                .returningResultSet("resulset",new CaminoMapper());
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idNodos", StringUtils.join(idNodos,","));
        
        Map resp = simpleJdbcCall.execute(params);
        
        return (List) resp.get("resulset");
        
    }
        
    
}
