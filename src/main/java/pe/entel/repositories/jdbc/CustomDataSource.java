/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.repositories.jdbc;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

/**
 *
 * @author dmontjoy
 */
public class CustomDataSource extends NamedParameterJdbcDaoSupport{
   @Autowired
   public void initConfiguration(DataSource datasource){
       this.setDataSource(datasource);
   }
    
}
