package pe.entel.repositories;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pe.entel.entities.Camino;


@RepositoryRestResource(path = "caminos", collectionResourceRel = "caminos")
public interface CaminoRepository extends JpaRepository<Camino, Long> {
    @RestResource(path = "allByRuta", rel="allByRuta")
    List<Camino> findAllByRutaContaining(@Param("ruta") String term); 
    
    
    @RestResource(path = "getCaminos", rel="getCaminos")
    @Query("select distinct camino from Camino camino"
            + " inner join camino.detalles pdetalle"
            + " where"
            + " (pdetalle.target.id in :idNodos or length(concat(:idNodos)) is null)"
    )       
    List<Camino> findByRuta(
            @Param("idNodos") Set<Long> idNodos);
    
       /*Set<Camino> findByRutaIn(
            @Param("nombreNodos") Set<String> nombreNodos);*/
   
}
