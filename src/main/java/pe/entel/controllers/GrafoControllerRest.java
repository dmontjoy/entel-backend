/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.entel.entities.Camino;
import pe.entel.repositories.jdbc.CaminoJdbcRepository;
/**
 *
 * @author dmontjoy
 */
@RepositoryRestController
public class GrafoControllerRest {
    private @Autowired CaminoJdbcRepository caminoJdbcRepository;
    @GetMapping("caminos/search/allByNameCamino")
    public @ResponseBody List<Camino> allByNameCamino(@RequestParam(name = "idNodo", required = false) List <String> idNodo){
        System.err.print("idNodo: "+idNodo);
        return caminoJdbcRepository.findAllByFilter(idNodo);
    }
}
