/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pe.entel.entities.Camino;
import pe.entel.entities.CaminoDetalle;
import pe.entel.entities.Nodo;
import pe.entel.repositories.CaminoDetalleRepository;
import pe.entel.repositories.NodoRepository;
import pe.entel.repositories.jdbc.CaminoJdbcRepository;


@Controller
public class GrafoController {
    private @Autowired CaminoDetalleRepository caminoDetalleRepository;
    private @Autowired NodoRepository nodoRepository;
    private @Autowired CaminoJdbcRepository caminoJdbcRepository;
    
    @GetMapping("grafo")
    public ModelAndView index(@RequestParam(name = "idNodo", required = false) List<String> ids){
        ModelAndView mav= new ModelAndView("grafo");
        List<Camino> caminos;
      

        caminos = caminoJdbcRepository.findAllByFilter(ids);

        List<CaminoDetalle> caminoDetalles = caminoDetalleRepository.findByCaminoIdIn(caminos.stream().map(Camino::getId).collect(Collectors.toList()));
        Set<Nodo> nodos = nodoRepository.findByIdIn(caminoDetalles.stream().map(CaminoDetalle::getTarget).map(Nodo::getId).collect(Collectors.toList()));
        
        Map node,link;
       
        List<Map> items= new ArrayList<>();
        
        for(Nodo nodo: nodos){
            node = new HashMap();
            node.put("id", nodo.getId());
            node.put("label", nodo.getNombre());
            items.add(node);
            
        }
         node = new HashMap();
         node.put("nodes", items);
        
        items = new ArrayList<>();
        for(CaminoDetalle caminoDetalle: caminoDetalles){
            if(caminoDetalle.getSource()!=null){
                link = new HashMap();
                link.put("source", caminoDetalle.getSource().getId());
                link.put("target", caminoDetalle.getTarget().getId());
                items.add(link);
            
            }
        }
         //link = new HashMap();
         node.put("links", items);
         
        mav.addObject("json", node);
        return mav;
    }
    
    
     @GetMapping("grafoCamino")
    public ModelAndView grafoCamino(@RequestParam(name = "idCamino", required = false) List <Long> id){
        ModelAndView mav= new ModelAndView("grafo");
        System.err.println(id);
        List<CaminoDetalle> caminoDetalles;
        
        if(id==null){
             caminoDetalles = caminoDetalleRepository.findAll();

        }else{
           
            caminoDetalles = caminoDetalleRepository.findByCaminoIdIn(id);

        }    
   
        Set<Nodo> nodos = nodoRepository.findByIdIn(caminoDetalles.stream().map(CaminoDetalle::getTarget).map(Nodo::getId).collect(Collectors.toList()));
        
        Map node,link;
       
        List<Map> items= new ArrayList<>();
        
        for(Nodo nodo: nodos){
            node = new HashMap();
            node.put("id", nodo.getId());
            node.put("label", nodo.getNombre());
            items.add(node);
            
        }
         node = new HashMap();
         node.put("nodes", items);
        
        items = new ArrayList<>();
        for(CaminoDetalle caminoDetalle: caminoDetalles){
            if(caminoDetalle.getSource()!=null){
                link = new HashMap();
                link.put("source", caminoDetalle.getSource().getId());
                link.put("target", caminoDetalle.getTarget().getId());
                items.add(link);
            
            }
        }
         //link = new HashMap();
         node.put("links", items);
         
        mav.addObject("json", node);
        return mav;
    }
  

    
}
