package pe.entel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntelDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntelDemoApplication.class, args);
	}
}
