/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import pe.entel.entities.Camino;


/**
 *
 * @author dmontjoy
 */
public class CaminoMapper implements RowMapper<Camino>{

    @Override
    public Camino mapRow(ResultSet rs, int i) throws SQLException {
        System.err.println(rs.toString());
        Camino camino = new Camino(Long.parseLong(rs.getString("id")) );
        camino.setNombre(rs.getString("nombre"));
        camino.setFin(rs.getString("fin"));
        camino.setInicio(rs.getString("inicio"));
        camino.setRuta(rs.getString("ruta"));
        camino.setTipo(rs.getString("tipo"));
        
        return camino;
    }
    
    
    
    
}
