package pe.entel.config;

import java.util.Set;
import java.util.regex.Pattern;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

@Component
public class SpringDataRestCustomization extends RepositoryRestConfigurerAdapter {
    
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        /*config.setBasePath("/api");*/
        config
            .getCorsRegistry()
            .addMapping("/**")
            .allowedMethods("GET", "HEAD", "POST", "DELETE", "OPTIONS")
            .allowedOrigins(
                    "http://localhost:4200")
            ;
        
        final ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));

        final Set<BeanDefinition> beans = provider.findCandidateComponents("pe.entel.entities");

        beans.forEach((bean) -> {
            Class<?> idExposedClasses;
            try {
                idExposedClasses = Class.forName(bean.getBeanClassName());
                config.exposeIdsFor(Class.forName(idExposedClasses.getName()));
            }
            catch (ClassNotFoundException e) {
                throw new RuntimeException("Failed to expose `id` field due to", e);
            }
        });
    }
}
