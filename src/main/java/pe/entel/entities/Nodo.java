/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.entities;
import java.io.Serializable;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 *
 * @author dmontjoy
 */
@Entity
@Getter @Setter @NoArgsConstructor
public class Nodo extends AbstractEntity implements Serializable  {
    private String nombre;
}
