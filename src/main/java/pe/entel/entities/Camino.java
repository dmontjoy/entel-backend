package pe.entel.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter @Setter @NoArgsConstructor
public class Camino extends AbstractEntity implements Serializable {
    private String nombre;
    private String inicio;
    private String fin;
    private String tipo;
    @Lob
    private String ruta;
    @OneToMany(mappedBy = "camino")
    @JsonManagedReference
    private Set<CaminoDetalle> detalles;
    
    public Camino(Long id){
        super(id);
    }
    
}
