/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.entel.entities.projections;

import java.util.Set;
import org.springframework.data.rest.core.config.Projection;
import pe.entel.entities.Camino;
import pe.entel.entities.CaminoDetalle;

@Projection(name = "camino", types = { Camino.class })
public interface CaminoProjection {
    Long getId();
    String getNombre();
    String getInicio();
    String getFin();
    String getTipo();
    String getRuta();
    Set<CaminoDetalle> getDetalles();
}
