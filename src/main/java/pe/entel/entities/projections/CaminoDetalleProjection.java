package pe.entel.entities.projections;

import org.springframework.data.rest.core.config.Projection;
import pe.entel.entities.CaminoDetalle;
import pe.entel.entities.Nodo;

@Projection(name = "nodes", types = { CaminoDetalle.class })
public interface CaminoDetalleProjection {
    Long getId();
    Nodo getSource();
    Nodo getTarget();
    String getRuta();
    int getOrden();
}
