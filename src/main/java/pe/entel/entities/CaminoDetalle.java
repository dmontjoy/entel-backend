package pe.entel.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @NoArgsConstructor
public class CaminoDetalle extends AbstractEntity implements Serializable {
    @ManyToOne
    @JoinColumn(name = "idcamino", nullable = false)
    @JsonBackReference
    private Camino camino;
    private String iduIn;
    private String iduOut;
    @Lob
    private String ruta;
    private int orden;
    @ManyToOne
    @JoinColumn(name = "idnodo_in")
    private Nodo source;
    @ManyToOne
    @JoinColumn(name = "idnodo_out")
    private Nodo target;
}
